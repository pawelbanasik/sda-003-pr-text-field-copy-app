package com.example.rent.sda_003_pr_text_field_copy_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editTextOne;
    private TextView textViewOne;
    private Button buttonOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextOne = (EditText) findViewById(R.id.edit_text_one);
        textViewOne = (TextView) findViewById(R.id.text_view_one);
        buttonOne = (Button) findViewById(R.id.button_one);


    }

    public void clickButton(View v) {
        textViewOne.setText(editTextOne.getText());
    }
}
